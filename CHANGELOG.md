# CHANGELOG

<!--- next entry here -->

## 0.2.0
2020-08-13

### Features

- add enum for rabbitmq header (436d90b6bdc38aa91a84f5c37dda1e12f929d30e)

## 0.1.3
2020-08-12

### Fixes

- add db attribute (0b8df99185e6179e04c61092b90e9ea0bdde99e2)

## 0.1.2
2020-07-31

### Fixes

- fix update lib (c8271dd34fb7c1ce9014f2db5eece316da7843dd)

## 0.1.1
2020-07-31

### Fixes

- fix unmarshal (5d89b5b6be071accc35cac4ccac8d6b676ace1d7)

## 0.1.0
2020-07-12

### Features

- initial commit (474e44e149f985ad05e52456c6e3d6fd52e773f1)