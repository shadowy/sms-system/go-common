package model

import (
	"encoding/json"
	"github.com/sirupsen/logrus"
)

type DeliveryStatus struct {
	ID     int64   `json:"id"`
	Status string  `json:"status"`
	Error  *string `json:"error"`
}

func (message DeliveryStatus) ToByteArray() (data []byte, err error) {
	data, err = json.Marshal(message)
	if err != nil {
		logrus.WithError(err).Error("model.DeliveryStatus.ToByteArray")
		return
	}

	return data, nil
}

func (message DeliveryStatus) Parse(data []byte) (status *DeliveryStatus, err error) {
	result := DeliveryStatus{}
	if err = json.Unmarshal(data, &result); err != nil {
		logrus.WithError(err).Error("model.DeliveryStatus.Parse")
		return
	}

	return &result, err
}
