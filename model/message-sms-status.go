package model

import (
	"encoding/json"
	"github.com/sirupsen/logrus"
	"time"
)

type MessageSmsStatus struct {
	Command string             `json:"status"` /*error_send, sending, error_deliver, delivered*/
	ID      *int64             `json:"id"`
	MsgID   *string            `json:"msgId"`
	Error   *string            `json:"error"`
	Time    time.Time          `json:"time"`
	Sms     *MessageSmsForSend `json:"sms"`
}

func (sms MessageSmsStatus) ToByteArray() (data []byte, err error) {
	if data, err = json.Marshal(sms); err != nil {
		logrus.WithError(err).Error("model.MessageSmsStatus.ToByteArray")
	}

	return data, err
}

func (sms MessageSmsStatus) Parse(data []byte) (status *MessageSmsStatus, err error) {
	result := MessageSmsStatus{}
	if err = json.Unmarshal(data, &result); err != nil {
		logrus.WithError(err).Error("MessageSmsStatus.Parse")
		return
	}

	if result.MsgID != nil && *result.MsgID == "" {
		result.MsgID = nil
	}

	return &result, err
}
