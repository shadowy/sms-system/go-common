package model

import (
	"encoding/json"
	"github.com/sirupsen/logrus"
	"time"
)

type MessageNewSms struct {
	InterfaceCode string    `json:"interface"`
	From          string    `json:"from"`
	To            string    `json:"to"`
	Text          string    `json:"text"`
	Sn            string    `json:"sn"`
	Umr           string    `json:"umr"`
	ExtInfo       string    `json:"extInfo"`
	Time          time.Time `json:"time"`
}

func (message *MessageNewSms) ToByteArray() (data []byte, err error) {
	data, err = json.Marshal(message)
	if err != nil {
		logrus.WithError(err).Error("model.MessageNewSms.ToByteArray")
		return
	}

	return data, nil
}

func (message *MessageNewSms) Parse(data []byte) (status *MessageNewSms, err error) {
	result := MessageNewSms{}
	if err = json.Unmarshal(data, &result); err != nil {
		logrus.WithError(err).Error("model.MessageNewSms.Parse")
		return
	}

	return &result, nil
}
