package model

import (
	"encoding/json"
	"github.com/sirupsen/logrus"
)

type SmsForDelivery struct {
	ID        int64  `json:"id"`
	From      string `json:"from"`
	To        string `json:"to"`
	Text      string `json:"text"`
	Transport struct {
		URL    string `json:"url"`
		Method string `json:"method"`
		Type   string `json:"type"`
		Params []struct {
			Name string `json:"name"`
			Type string `json:"type"`
		}
	} `json:"transport"`
}

func (message *SmsForDelivery) ToByteArray() (data []byte, err error) {
	if data, err = json.Marshal(message); err != nil {
		logrus.WithError(err).Error("model.SmsForDelivery.ToByteArray")
	}

	return
}

func (message *SmsForDelivery) Parse(data []byte) (status *SmsForDelivery, err error) {
	result := SmsForDelivery{}
	if err = json.Unmarshal(data, &result); err == nil {
		status = &result
	} else {
		logrus.WithError(err).Error("model.SmsForDelivery.Parse")
	}

	return
}

func (message *SmsForDelivery) Params() logrus.Fields {
	return logrus.Fields{
		"id":               message.ID,
		"from":             message.From,
		"to":               message.To,
		"text":             message.Text,
		"transport-url":    message.Transport.URL,
		"transport-method": message.Transport.Method,
		"transport-type":   message.Transport.Type,
	}
}
