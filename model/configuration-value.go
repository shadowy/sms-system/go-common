package model

type ConfigurationValue struct {
	Code  string `db:"cfg_code"`
	Value string `db:"cfg_value"`
}
