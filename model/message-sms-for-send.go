package model

import (
	"encoding/json"
	"github.com/sirupsen/logrus"
)

type MessageSmsForSend struct {
	ID        int64      `json:"id" db:"msg_id"`
	Interface string     `json:"interface" db:"interface_code"`
	From      string     `json:"from" db:"msg_from"`
	To        string     `json:"to" db:"msg_to"`
	Text      string     `json:"text" db:"msg_text"`
	ReplyInfo *ReplyInfo `json:"replyInfo" db:"reply"`
	TaskID    *int64     `json:"taskId" db:"task_id"`
	Priority  int64      `json:"priority" db:"priority"`
}

func (message *MessageSmsForSend) ToByteArray() (data []byte, err error) {
	if data, err = json.Marshal(message); err != nil {
		logrus.WithError(err).Error("model.MessageSmsForSend.ToByteArray")
		return
	}

	return data, nil
}

func (message *MessageSmsForSend) Parse(data []byte) (status *MessageSmsForSend, err error) {
	result := MessageSmsForSend{}
	if err = json.Unmarshal(data, &result); err != nil {
		logrus.WithError(err).Error("model.MessageSmsForSend.Parse")
		return nil, err
	}

	return &result, nil
}
