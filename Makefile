all: test lint

test:
	@echo ">> test"
	@go test ./... -coverprofile cover.out
	@go tool cover -html=cover.out -o cover.html

lint: lint-golang-ci lint-card

lint-card:
	@echo ">> lint-card"
	@goreportcard-cli -v

lint-golang-ci:
	@echo ">> lint-golang-ci"
	@golangci-lint run
